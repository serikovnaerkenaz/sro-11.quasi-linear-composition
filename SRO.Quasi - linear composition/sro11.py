import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder

# Датасетті жүктеу
file_path = 'C:/Users/Yerkenaz/Desktop/сро моис/student_data.csv'
df = pd.read_csv(file_path)

# Мақсатты айнымалыны таңдау
target_variable = 'studytime'

# Категориялық айнымалыларды сандық форматқа түрлендіру
le = LabelEncoder()
for column in df.select_dtypes(include='object').columns:
    df[column] = le.fit_transform(df[column])

# Белгілерге және мақсатты айнымалыға бөлу
X = df.drop(target_variable, axis=1)
y = df[target_variable]

# Оқу және тест жиынтықтарына бөлу
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Негізгі алгоритм - кездейсоқ орман
rf_model = RandomForestRegressor(n_estimators=100, random_state=42)
rf_model.fit(X_train, y_train)

# Тест жиынтығындағы болжамдар
rf_predictions = rf_model.predict(X_test)

# Екінші алгоритм - градиентті бустинг
gb_model = GradientBoostingRegressor(n_estimators=100, learning_rate=0.1, max_depth=3, random_state=42)
gb_model.fit(X_train, y_train)

# Тест жиынтығындағы болжамдар
gb_predictions = gb_model.predict(X_test)

# Квази-сызықтық композиция-екі алгоритмнің болжамдарын қосамыз
quasi_linear_predictions = rf_predictions + gb_predictions

# Сапасын бағалау
mse = mean_squared_error(y_test, quasi_linear_predictions)
print(f'Mean Squared Error (MSE) на тестовом наборе: {mse}')
